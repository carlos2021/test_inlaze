from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action

from inlaze_back.message.models import Order
from inlaze_back.message.api.serializers.orderSerializer import OrderSerializer

from django.utils.datastructures import MultiValueDictKeyError
from datetime import date

from datetime import date


class OrderViewSet(viewsets.ModelViewSet):
    model = Order
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    http_method_names = ["get", "post"]

    def create(self, request):
        """
        Agendar un pedido a un conductor en una fecha y hora, y especificar su lugar de
        recogida (latitud y longitud) y destino.
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=["get"])
    def per_day(self, request, day=None, mounth=None, year=None):
        """
        Consultar todos los pedidos asignados en un día en específico ordenados por la hora.
        como parametros recibe
            year : int
            mounth : int
            day : int
        url de ejemplo /api-message/order/per_day/?year=2021&mounth=12&day=12
        """
        try:
            year = int(request.query_params["year"])
            mounth = int(request.query_params["mounth"])
            day = int(request.query_params["day"])
        except MultiValueDictKeyError:
            return Response(
                {
                    "message_error": "necesitas agregar los query parameter mira la documentación",
                    "message": "desde el swagger no le podes agregar los query, hazolo desde insomnia o postman",
                }
            )
        queryset = self.model.objects.filter(
            scheduling_date__contains=date(year, mounth, day)
        ).order_by("scheduling_date")
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["get"])
    def per_day_and_driver(self, request, *args, **kwgars):
        """
        Consultar todos los pedidos de un conductor en un día en específico ordenados por
        la hora.
        como parametros recibe
            year : int
            mounth : int
            day : int
            id_driver   : int

        url de ejemplo /api-message/order/per_day_and_driver/?year=2021&mounth=12&day=12&id_driver=1
        """
        try:
            year = int(request.query_params["year"])
            mounth = int(request.query_params["mounth"])
            day = int(request.query_params["day"])
            id_driver = int(request.query_params["id_driver"])
        except MultiValueDictKeyError:
            return Response(
                {
                    "message_error": "necesitas agregar los query parameter mira la documentación",
                    "message": "desde el swagger no le podes agregar los query, hazolo desde insomnia o postman",
                }
            )

        queryset = self.model.objects.filter(
            scheduling_date__contains=date(year, mounth, day), message=id_driver
        ).order_by("scheduling_date")
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)
