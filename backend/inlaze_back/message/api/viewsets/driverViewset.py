from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action

from inlaze_back.message.models import message
from inlaze_back.message.api.serializers.driverSerializer import DriverSerializer


from django.utils.datastructures import MultiValueDictKeyError
from inlaze_back.message.utilites import (
    get_ubication_drivers,
    update_ubication_driver,
    driver_nearest,
)


class DriverViewSet(viewsets.ModelViewSet):
    model = message
    serializer_class = DriverSerializer
    queryset = message.objects.all()
    http_method_names = ["get"]

    @action(detail=False, methods=["get"])
    def update_ubication(self, request):
        """
        Endpoint para que los datos de ubicación de los conductores se generen, ya que se encuentran
        almacenados en otro sistema, el cual debe ser consultado por este cada cierto tiempo para
        actualizar la ubicación de estos.
        """
        ubication = get_ubication_drivers()
        is_success = update_ubication_driver(ubication)
        return Response({"update_successfully": is_success})

    @action(detail=False, methods=["get"])
    def nearest(self, request):
        """
        Hacer búsquedas del conductor que esté más cerca de un punto geográfico en una
        fecha y hora. (Tener en consideración los pedidos ya asignados al conductor).
        como parametros recibe
            lat : int
            lon : int
            datetime_search : datetime

        url de ejemplo:
            /api-message/message/nearest/?datetime_search=2022-11-03T20%3A59%3A44.005Z&lat=1&lon=1
        """
        try:
            lat = int(request.query_params["lat"])
            lon = int(request.query_params["lon"])
            datetime_search = request.query_params["datetime_search"]
        except MultiValueDictKeyError:
            return Response(
                {
                    "message_error": "necesitas agregar los query parameter mira la documentación",
                    "message": "desde el swagger no le podes agregar los query, hazolo desde insomnia o postman",
                }
            )
        order_by_cercan = driver_nearest(self.queryset, lat, lon, datetime_search)
        serializer = self.serializer_class(order_by_cercan, many=False)
        return Response(serializer.data)
