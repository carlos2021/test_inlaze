from rest_framework.routers import DefaultRouter

from inlaze_back.message.api.viewsets.driverViewset import DriverViewSet
from inlaze_back.message.api.viewsets.orderViewset import OrderViewSet


router = DefaultRouter()

router.register("order", OrderViewSet, basename="order")
router.register("message", DriverViewSet, basename="message")

urlpatterns = router.urls
