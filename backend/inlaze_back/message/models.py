from django.db import models


class Message(models.Model):
    work_latitude = models.IntegerField(blank=True, null=True)
    work_length = models.IntegerField(blank=True, null=True)
    last_update = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = "Mensaje"
        verbose_name_plural = "mensaje"
