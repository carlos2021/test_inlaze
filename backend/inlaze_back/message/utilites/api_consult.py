from utilites.constants import URL_API_UBICATION_DRIVERS
import requests


def get_ubication_drivers():
    response_ubication = requests.get(URL_API_UBICATION_DRIVERS)
    return response_ubication.json().get("alfreds", 0)
