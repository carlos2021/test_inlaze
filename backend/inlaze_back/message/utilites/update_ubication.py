from datetime import datetime
from inlaze_back.message.models import message


def update_ubication_driver(data_of_alfreds) -> bool:
    try:
        for element in data_of_alfreds:
            date_str = element["lastUpdate"]
            date_necesary = date_str[:10]
            date_dt = datetime.strptime(date_necesary, "%Y-%m-%d")
            instance, _ = message.objects.get_or_create(alfred=element["id"])
            instance.work_latitude = element["lat"]
            instance.work_length = element["lng"]
            instance.last_update = date_dt
            instance.save()
    except:
        return False
    return True
