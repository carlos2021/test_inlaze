from datetime import datetime
from datetime import timedelta
from inlaze_back.message.models import Order
from django.shortcuts import get_list_or_404
from django.http import Http404
from utilites.constants import HOUR_DURATION_ORDER


def is_availability_of_driver(message, date_consult) -> bool:
    try:
        orders = get_list_or_404(Order, message=message)
        list_scheduled_dates = [order.scheduling_date for order in orders]
        is_availble_date = is_avaible_date_in_list(date_consult, list_scheduled_dates)
        return is_availble_date
    except Http404:
        return False


def is_avaible_date_in_list(date_consult, list_scheduled_dates):
    exists_menor_to_range = {
        (date_consult - date_scheduled) >= timedelta(hours=HOUR_DURATION_ORDER)
        if (date_consult >= date_scheduled)
        else (date_scheduled - date_consult) >= timedelta(hours=HOUR_DURATION_ORDER)
        for date_scheduled in list_scheduled_dates
    }
    return False in exists_menor_to_range
