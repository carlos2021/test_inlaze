import jwt
from inlaze_back.users.models import User


def getUserByTokenJWT(request):
    token = request.headers["Authorization"].replace("Bearer ", "")
    decode_token = jwt.decode(token, options={"verify_signature": False})
    usuer_actual = User.objects.get(pk=int(decode_token["user_id"]))
    return usuer_actual
