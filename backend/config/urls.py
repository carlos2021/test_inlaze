from django.contrib import admin
from django.urls import path, include
from config.config_swagger import urlpatternsSwagger

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from inlaze_back.users.views import Login,Logout, RegisterApi


urlpatterns = urlpatternsSwagger + [
    path('auth/register/',RegisterApi.as_view(), name = 'register'), 
    path('auth/login/',Login.as_view(), name = 'login'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),  
    path("api-message/", include("inlaze_back.message.api.routers")),
]
