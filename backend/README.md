# inlaze_back!

Este es el backend de la prueba para el proceso de Selección Inlaze

## Settings

Usa las variables y constantes de configuración, detalladas en la documentación: [configuraciones](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Docker

El proyecto esta fuertemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. Para mas detalles, verificar los comandos en [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

## Comandos basicos de trabajo

### Configurando usarios

-   Para crear una cuenta de **usuario normal**, se debe registrar por medio de la url register dando los dato del usuario

## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up

## Autentificar rutas
    $ registrese, luego has login y le da un token
    $ le da en el candado escribe "Bearer {token}"

    
### Migraciones

    $ docker-compose -f local.yml run --rm django python manage.py makemigrations
    $ docker-compose -f local.yml run --rm django python manage.py migrate

### Validación de tipado

Running type checks with mypy:

    $ docker-compose -f local.yml run --rm django mypy inlaze_back

### Análisis de código estatico

El proyecto incluye Flake8, Bandit, y PyLint, para los dos primeros, solo es correr el proceso directamente en el archivo del proyecto. Para el caso de PyLint, si se hace necesaria la configuración de las variables del proyecto, por lo que se corre directamente en Docker.

    $ docker-compose -f local.yml run --rm django pylint inlaze_back

### Cobertura de pruebas

Para correr las pruebas, es importante usar la cobertura de los mismos, lo que ademas permite visualizar en resultado en un reporte HTML:

    $ docker-compose -f local.yml run --rm django coverage run -m pytest
    $ docker-compose -f local.yml run --rm django coverage html
    $ open htmlcov/index.html

#### Correr los test usando pytest

    $ docker-compose -f local.yml run --rm django pytest


## Despliegue a producción

Para mayor información sobre el despliegue del proyecto en entornos de producción, revisar la documentación [usando Docker](https://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html)
